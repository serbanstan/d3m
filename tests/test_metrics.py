import io
import unittest

import pandas

from d3m import exceptions, metrics


class TestMetrics(unittest.TestCase):
    def _read_csv(self, csv):
        return pandas.read_csv(
            io.StringIO(csv),
            # We do not want to do any conversion of values at this point.
            # This should be done by primitives later on.
            dtype=str,
            # We always expect one row header.
            header=0,
            # We want empty strings and not NaNs.
            na_filter=False,
            encoding='utf8',
        )

    def test_alignment(self):
        truth = self._read_csv("""
d3mIndex,class_label
1,a
2,b
3,c
4,d
        """)

        predictions = self._read_csv("""
d3mIndex,class_label,confidence
1,a,0.1
2,b,0.4
4,d,0.5
3,c,0.6
        """)

        self.assertEqual(metrics.Metric.align(truth, predictions).values.tolist(), [['1', 'a', '0.1'], ['2', 'b', '0.4'], ['3', 'c', '0.6'], ['4', 'd', '0.5']])

        predictions = self._read_csv("""
d3mIndex,confidence,class_label
1,0.1,a
2,0.4,b
4,0.5,d
3,0.6,c
        """)

        self.assertEqual(metrics.Metric.align(truth, predictions).values.tolist(), [['1', 'a', '0.1'], ['2', 'b', '0.4'], ['3', 'c', '0.6'], ['4', 'd', '0.5']])

        predictions = self._read_csv("""
confidence,class_label,d3mIndex
0.1,a,1
0.4,b,2
0.5,d,4
0.6,c,3
        """)

        self.assertEqual(metrics.Metric.align(truth, predictions).values.tolist(), [['1', 'a', '0.1'], ['2', 'b', '0.4'], ['3', 'c', '0.6'], ['4', 'd', '0.5']])

        predictions = self._read_csv("""
d3mIndex
1
2
4
3
        """)

        with self.assertRaises(exceptions.InvalidArgumentValueError):
            metrics.Metric.align(truth, predictions)

        predictions = self._read_csv("""
d3mIndex,class_label,confidence
1,a,0.1
2,b,0.4
3,c,0.6
        """)

        with self.assertRaises(exceptions.InvalidArgumentValueError):
            metrics.Metric.align(truth, predictions)

        truth = self._read_csv("""
d3mIndex,class_label
1,a1
1,a2
2,b
3,c1
3,c2
3,c3
4,d1
4,d2
        """)

        predictions = self._read_csv("""
d3mIndex,class_label
2,b
4,d
3,c
1,a
        """)

        self.assertEqual(metrics.Metric.align(truth, predictions).values.tolist(), [['1', 'a'], ['2', 'b'], ['3', 'c'], ['4', 'd']])

        predictions = self._read_csv("""
d3mIndex,class_label
4,d
2,b1
2,b2
2,b3
2,b4
2,b5
2,b6
3,c
1,a
        """)

        self.assertEqual(metrics.Metric.align(truth, predictions).values.tolist(), [['1', 'a'], ['2', 'b1'], ['2', 'b2'], ['2', 'b3'], ['2', 'b4'], ['2', 'b5'], ['2', 'b6'], ['3', 'c'], ['4', 'd']])

        truth = self._read_csv("""
d3mIndex,class_label
1,a1
1,a2
3,c1
2,b
3,c2
3,c3
4,d1
4,d2
        """)

        with self.assertRaises(exceptions.InvalidArgumentValueError):
            metrics.Metric.align(truth, predictions)

    def test_labels(self):
        pred_df = pandas.DataFrame(columns=['d3mIndex', 'class'], dtype=object)
        pred_df['d3mIndex'] = pandas.Series([0, 1, 2, 3, 4])
        pred_df['class'] = pandas.Series(['a', 'b', 'a', 'b', 'b'])

        ground_truth_df = pandas.DataFrame(columns=['d3mIndex', 'class'], dtype=object)
        ground_truth_df['d3mIndex'] = pandas.Series([0, 1, 2, 3, 4])
        ground_truth_df['class'] = pandas.Series(['a', 'b', 'a', 'b', 'a'])

        precision_metric = metrics.PrecisionMetric(pos_label='a')
        self.assertEqual(precision_metric.score(ground_truth_df, pred_df), 1.0)

        precision_metric = metrics.PrecisionMetric(pos_label='b')
        self.assertAlmostEqual(precision_metric.score(ground_truth_df, pred_df), 0.6666666666666666)

    def test_hamming_loss(self):
        # Testcase 1: MultiLabel, typical

        y_true = self._read_csv("""
d3mIndex,class_label
3,happy-pleased
3,relaxing-calm
7,amazed-suprised
7,happy-pleased
13,quiet-still
13,sad-lonely
        """)

        y_pred = self._read_csv("""
d3mIndex,class_label
3,happy-pleased
3,sad-lonely
7,amazed-suprised
7,happy-pleased
13,quiet-still
13,happy-pleased
        """)

        self.assertAlmostEqual(metrics.HammingLossMetric().score(y_true, y_pred), 0.26666666666666666)

        # Testcase 2: MultiLabel, Zero loss

        y_true = self._read_csv("""
d3mIndex,class_label
3,happy-pleased
3,relaxing-calm
7,amazed-suprised
7,happy-pleased
13,quiet-still
13,sad-lonely
        """)

        y_pred = self._read_csv("""
d3mIndex,class_label
3,happy-pleased
3,relaxing-calm
7,amazed-suprised
7,happy-pleased
13,quiet-still
13,sad-lonely
        """)

        self.assertAlmostEqual(metrics.HammingLossMetric().score(y_true, y_pred), 0.0)

        # Testcase 3: MultiLabel, Complete loss

        y_true = self._read_csv("""
d3mIndex,class_label
3,happy-pleased
3,relaxing-calm
7,amazed-suprised
7,happy-pleased
13,quiet-still
13,sad-lonely
        """)

        y_pred = self._read_csv("""
d3mIndex,class_label
3,ecstatic
3,sad-lonely
3,quiet-still
3,amazed-suprised
7,ecstatic
7,sad-lonely
7,relaxing-calm
7,quiet-still
13,ecstatic
13,happy-pleased
13,relaxing-calm
13,amazed-suprised
        """)

        self.assertAlmostEqual(metrics.HammingLossMetric().score(y_true, y_pred), 1.0)

        # Testcase 4: Multiclass, case 1
        # Multiclass is not really supported or reasonable to use, but we still test it to test also edge cases.

        y_true = self._read_csv("""
d3mIndex,species
1,versicolor
2,versicolor
16,virginica
17,setosa
22,versicolor
26,versicolor
30,versicolor
31,virginica
33,versicolor
37,virginica
        """)

        y_pred = self._read_csv("""
d3mIndex,species
1,setosa
2,versicolor
16,virginica
17,setosa
22,versicolor
26,virginica
30,versicolor
31,virginica
33,versicolor
37,virginica
        """)

        self.assertAlmostEqual(metrics.HammingLossMetric().score(y_true, y_pred), 0.1333333)

         # Testcase 5: Multiclass, case 2
        # Multiclass is not really supported or reasonable to use, but we still test it to test also edge cases.

        y_true = self._read_csv("""
d3mIndex,species
1,versicolor
2,versicolor
16,virginica
17,setosa
22,versicolor
26,versicolor
30,versicolor
31,virginica
33,versicolor
37,virginica
        """)

        y_pred = self._read_csv("""
d3mIndex,species
1,versicolor
2,versicolor
16,virginica
17,setosa
22,versicolor
26,versicolor
30,versicolor
31,virginica
33,versicolor
37,virginica
        """)

        self.assertAlmostEqual(metrics.HammingLossMetric().score(y_true, y_pred), 0.0)

        # Testcase 6: Multiclass, case 3
        # Multiclass is not really supported or reasonable to use, but we still test it to test also edge cases.

        y_true = self._read_csv("""
d3mIndex,species
1,versicolor
2,versicolor
16,versicolor
17,virginica
22,versicolor
26,versicolor
30,versicolor
31,virginica
33,versicolor
37,virginica
        """)

        y_pred = self._read_csv("""
d3mIndex,species
1,setosa
2,setosa
16,setosa
17,setosa
22,setosa
26,setosa
30,setosa
31,setosa
33,setosa
37,setosa
        """)

        self.assertAlmostEqual(metrics.HammingLossMetric().score(y_true, y_pred), 0.66666666)

    def test_root_mean_squared_error(self):
        y_true = self._read_csv("""
d3mIndex,value
1,3
2,-1.0
16,2
17,7
        """)

        y_pred = self._read_csv("""
d3mIndex,value
1,2.1
2,0.0
16,2
17,8
        """)

        self.assertAlmostEqual(metrics.RootMeanSquareErrorMetric().score(y_true, y_pred), 0.8381527307120105)

        y_true = self._read_csv("""
d3mIndex,value1,value2
1,0.5,1
2,-1,1
16,7,-6
        """)

        y_pred = self._read_csv("""
d3mIndex,value1,value2
1,0,2
2,-1,2
16,8,-5
        """)

        self.assertAlmostEqual(metrics.RootMeanSquareErrorMetric().score(y_true, y_pred), 0.8227486121839513)

    def test_precision_at_top_k(self):
        ground_truth_list_1 = self._read_csv("""
d3mIndex,value
1,1
2,10
3,7
4,5
5,12
6,6
7,2
8,18
9,4
10,8
        """)
        predictions_list_1 = self._read_csv("""
d3mIndex,value
1,0
2,2
3,8
4,6
5,14
6,9
7,3
8,17
9,10
10,11
        """)
        self.assertAlmostEqual(metrics.PrecisionAtTopKMetric(k=5).score(ground_truth_list_1, predictions_list_1), 0.6)


    def test_object_detection_average_precision(self):
        predictions_list_1 = self._read_csv("""
d3mIndex,box,confidence
1,"110,110,110,210,210,210,210,110",0.6
2,"5,10,5,20,20,20,20,10",0.9
2,"120,130,120,200,200,200,200,130",0.6
        """)

        ground_truth_list_1 = self._read_csv("""
d3mIndex,box
1,"100,100,100,200,200,200,200,100"
2,"10,10,10,20,20,20,20,10"
2,"70,80,70,150,140,150,140,80"
        """)

        self.assertAlmostEqual(metrics.ObjectDetectionAveragePrecisionMetric().score(ground_truth_list_1, predictions_list_1), 0.6666666666666666)

        predictions_list_2 = self._read_csv("""
d3mIndex,box,confidence
285,"330,463,330,505,387,505,387,463",0.0739
285,"420,433,420,498,451,498,451,433",0.0910
285,"328,465,328,540,403,540,403,465",0.1008
285,"480,477,480,522,508,522,508,477",0.1012
285,"357,460,357,537,417,537,417,460",0.1058
285,"356,456,356,521,391,521,391,456",0.0843
225,"345,460,345,547,415,547,415,460",0.0539
225,"381,362,381,513,455,513,455,362",0.0542
225,"382,366,382,422,416,422,416,366",0.0559
225,"730,463,730,583,763,583,763,463",0.0588
        """)

        ground_truth_list_2 = self._read_csv("""
d3mIndex,box
285,"480,457,480,529,515,529,515,457"
285,"480,457,480,529,515,529,515,457"
225,"522,540,522,660,576,660,576,540"
225,"739,460,739,545,768,545,768,460"
        """)

        self.assertAlmostEqual(metrics.ObjectDetectionAveragePrecisionMetric().score(ground_truth_list_2, predictions_list_2), 0.125)

        predictions_list_3 = self._read_csv("""
d3mIndex,box,confidence
1,"110,110,110,210,210,210,210,110",0.6
2,"120,130,120,200,200,200,200,130",0.6
2,"5,8,5,16,15,16,15,8",0.9
2,"11,12,11,18,21,18,21,12",0.9
        """)

        ground_truth_list_3 = self._read_csv("""
d3mIndex,box
1,"100,100,100,200,200,200,200,100"
2,"10,10,10,20,20,20,20,10"
2,"70,80,70,150,140,150,140,80"
        """)

        self.assertAlmostEqual(metrics.ObjectDetectionAveragePrecisionMetric().score(ground_truth_list_3, predictions_list_3), 0.4444444444444444)

        predictions_list_4 = self._read_csv("""
d3mIndex,box,confidence
1,"110,110,110,210,210,210,210,110",0.6
2,"120,130,120,200,200,200,200,130",0.6
2,"11,12,11,18,21,18,21,12",0.9
2,"5,8,5,16,15,16,15,8",0.9
        """)

        ground_truth_list_4 = self._read_csv("""
d3mIndex,box
1,"100,100,100,200,200,200,200,100"
2,"10,10,10,20,20,20,20,10"
2,"70,80,70,150,140,150,140,80"
        """)

        self.assertAlmostEqual(metrics.ObjectDetectionAveragePrecisionMetric().score(ground_truth_list_4, predictions_list_4), 0.4444444444444444)


if __name__ == '__main__':
    unittest.main()
