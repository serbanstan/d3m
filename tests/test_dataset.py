import filecmp
import glob
import json
import os
import os.path
import shutil
import tempfile
import unittest
import uuid

from sklearn import datasets

from d3m import utils, exceptions
from d3m.container import dataset, pandas
from d3m.metadata import base as metadata_base


def convert_metadata(metadata):
    return json.loads(json.dumps(metadata, cls=utils.JsonEncoder))


def _normalize_dataset_description(dataset_description, dataset_name=None):
    for key in ('digest', 'datasetVersion', 'datasetSchemaVersion', 'redacted'):
        dataset_description['about'].pop(key, None)

    for i, r in enumerate(dataset_description.get('dataResources', [])):
        for j, c in enumerate(r.get('columns', [])):
            if 'attribute' in c['role'] and len(c['role']) > 1:
                k = c['role'].index('attribute')
                c['role'].pop(k)
                dataset_description['dataResources'][i]['columns'][j] = c

    if dataset_name == 'audio_dataset_1':
        del dataset_description['dataResources'][1]['columns'][2]['refersTo']
        del dataset_description['dataResources'][1]['columns'][3]['refersTo']

    if dataset_name == 'dataset_TEST':
        dataset_description['about']['datasetID'] = 'object_dataset_1_TEST'

    return dataset_description


class TestDataset(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_dir)

    def test_d3m(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self._test_d3m(ds, dataset_doc_path)

    def _test_d3m(self, ds, dataset_doc_path):
        ds.metadata.check(ds)

        for row in ds['learningData']:
            for cell in row:
                # Nothing should be parsed from a string.
                self.assertIsInstance(cell, str, dataset_doc_path)

        self.assertEqual(len(ds['learningData']), 150, dataset_doc_path)
        self.assertEqual(len(ds['learningData'].iloc[0]), 6, dataset_doc_path)

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'iris_dataset_1',
            'version': '1.0',
            'name': 'Iris Dataset',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'source': {
                'license': 'CC',
                'redacted': False,
                'human_subjects_research': False,
            },
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': 'b5cda6740bae31be252316e877773a62307c44b13209af5eab30321e1a28df84',
        }, dataset_doc_path)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        }, dataset_doc_path)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 6,
            }
        }, dataset_doc_path)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        }, dataset_doc_path)

        for i in range(1, 5):
            self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, i))), {
                'name': ['sepalLength', 'sepalWidth', 'petalLength', 'petalWidth'][i - 1],
                'structural_type': 'str',
                'semantic_types': [
                    'http://schema.org/Float',
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            }, dataset_doc_path)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 5))), {
            'name': 'species',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        }, dataset_doc_path)

    def test_d3m_lazy(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path), lazy=True)

        ds.metadata.check(ds)

        self.assertTrue(len(ds) == 0)
        self.assertTrue(ds.is_lazy())

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'iris_dataset_1',
            'version': '1.0',
            'name': 'Iris Dataset',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'source': {
                'license': 'CC',
                'redacted': False,
                'human_subjects_research': False,
            },
            'dimension': {
                'length': 0,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': 'b5cda6740bae31be252316e877773a62307c44b13209af5eab30321e1a28df84',
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {})

        ds.load_lazy()

        self.assertFalse(ds.is_lazy())

        self._test_d3m(ds, dataset_doc_path)

    def test_d3m_saver(self):
        at_least_one = False

        for dirpath, dirnames, filenames in os.walk(os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets'))):
            if 'datasetDoc.json' in filenames:
                # Do not traverse further (to not parse "datasetDoc.json" or "problemDoc.json" if they
                # exists in raw data filename).
                dirnames[:] = []

                dataset_path = os.path.join(os.path.abspath(dirpath), 'datasetDoc.json')
                dataset_name = dataset_path.split(os.path.sep)[-2]

                # We are skipping "graph_dataset_1" because it contains a GML file which is not supported for saving.
                # See: https://gitlab.com/datadrivendiscovery/d3m/issues/349
                if not 'graph_dataset_1' in dataset_path:
                    self._test_d3m_saver(dataset_path, dataset_name)
                at_least_one = True

        self.assertTrue(at_least_one)

    def test_d3m_saver_update_column_description(self):
        source_dataset_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'audio_dataset_1'))
        source_dataset_uri = 'file://{dataset_path}'.format(dataset_path=os.path.join(source_dataset_path, 'datasetDoc.json'))

        output_dataset_path = os.path.join(self.test_dir, 'audio_dataset_1')
        output_dataset_uri = 'file://{dataset_path}'.format(dataset_path=os.path.join(output_dataset_path, 'datasetDoc.json'))

        selector = ('learningData', metadata_base.ALL_ELEMENTS, 0)
        new_metadata = {'description': 'Audio files'}
        ds = dataset.Dataset.load(source_dataset_uri)
        ds.metadata = ds.metadata.update(selector, new_metadata)
        ds.save(output_dataset_uri)
        ds2 = dataset.Dataset.load(output_dataset_uri)

        self.assertEqual(
            convert_metadata(ds.metadata.query(selector)),
            convert_metadata(ds2.metadata.query(selector))
        )

    def test_d3m_saver_file_columns(self):
        source_dataset_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'multivariate_dataset_1'))
        source_dataset_uri = 'file://{dataset_path}'.format(dataset_path=os.path.join(source_dataset_path, 'datasetDoc.json'))

        output_dataset_path = os.path.join(self.test_dir, 'multivariate_dataset_1')
        output_dataset_uri = 'file://{dataset_path}'.format(dataset_path=os.path.join(output_dataset_path, 'datasetDoc.json'))

        ds = dataset.Dataset.load(source_dataset_uri)
        ds.save(output_dataset_uri)

        with open(os.path.join(source_dataset_path, 'datasetDoc.json'), 'r') as f1, open(os.path.join(output_dataset_path, 'datasetDoc.json'), 'r') as f2:
            self.assertEqual(_normalize_dataset_description(json.load(f1)), _normalize_dataset_description(json.load(f2)))

        source_files = [x for x in glob.iglob(os.path.join(source_dataset_path, '**'), recursive=True) if os.path.isfile(x) and os.path.basename(x) != 'datasetDoc.json']
        output_files = [x for x in glob.iglob(os.path.join(output_dataset_path, '**'), recursive=True) if os.path.isfile(x) and os.path.basename(x) != 'datasetDoc.json']

        for x, y in zip(source_files, output_files):
            self.assertTrue(filecmp.cmp(x, y, shallow=False), (x, y))

        source_relative_filepaths = [os.path.relpath(x, source_dataset_path) for x in source_files]
        output_relative_filepaths = [os.path.relpath(x, output_dataset_path) for x in output_files]
        self.assertEqual(source_relative_filepaths, output_relative_filepaths)

    def test_load_sklearn_save_d3m(self):
        self.maxDiff = None

        for dataset_path in ['boston', 'breast_cancer', 'diabetes', 'digits', 'iris', 'linnerud']:
            source_dataset_uri = 'sklearn://{dataset_path}'.format(dataset_path=dataset_path)
            output_dateset_doc_path = os.path.join(self.test_dir, 'sklearn', dataset_path, 'datasetDoc.json')
            output_dateset_doc_uri = 'file://{output_dateset_doc_path}'.format(output_dateset_doc_path=output_dateset_doc_path)

            sklearn_dataset = dataset.Dataset.load(source_dataset_uri)
            sklearn_dataset.save(output_dateset_doc_uri)

            self.assertTrue(os.path.exists(output_dateset_doc_path))

            d3m_dataset = dataset.Dataset.load(output_dateset_doc_uri)

            sklearn_metadata = sklearn_dataset.metadata.to_internal_json_structure()
            d3m_metadata = d3m_dataset.metadata.to_internal_json_structure()

            del sklearn_metadata[0]['metadata']['location_uris']
            del d3m_metadata[0]['metadata']['location_uris']

            sklearn_metadata[0]['metadata']['source'] = d3m_metadata[0]['metadata']['source']
            sklearn_metadata[0]['metadata']['version'] = d3m_metadata[0]['metadata']['version']

            for metadata_index in range(3, len(sklearn_metadata)):
                sklearn_metadata[metadata_index]['metadata']['structural_type'] = 'str'

            self.assertEqual(sklearn_metadata, d3m_metadata)

    def test_load_csv_save_d3m(self):
        source_csv_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'tables', 'learningData.csv'))
        output_csv_path = os.path.join(self.test_dir, 'load_csv_save_d3m', 'iris_dataset_1', 'tables', 'learningData.csv')
        source_csv_uri = 'file://{source_csv_path}'.format(source_csv_path=source_csv_path)

        output_dateset_doc_path = os.path.join(self.test_dir, 'load_csv_save_d3m', 'iris_dataset_1', 'datasetDoc.json')
        output_dateset_doc_uri = 'file://{output_dateset_doc_path}'.format(output_dateset_doc_path=output_dateset_doc_path)

        csv_dataset = dataset.Dataset.load(source_csv_uri)
        csv_dataset.save(output_dateset_doc_uri)

        self.assertTrue(os.path.exists(output_dateset_doc_path))
        self.assertTrue(os.path.exists(output_csv_path))
        self.assertTrue(filecmp.cmp(source_csv_path, output_csv_path))

        d3m_dataset = dataset.Dataset.load(output_dateset_doc_uri)

        csv_metadata = csv_dataset.metadata.to_internal_json_structure()
        d3m_metadata = d3m_dataset.metadata.to_internal_json_structure()

        del csv_metadata[0]['metadata']['location_uris']
        del d3m_metadata[0]['metadata']['location_uris']
        del csv_metadata[0]['metadata']['stored_size']
        del d3m_metadata[0]['metadata']['approximate_stored_size']

        csv_metadata[0]['metadata']['source'] = d3m_metadata[0]['metadata']['source']
        csv_metadata[0]['metadata']['version'] = d3m_metadata[0]['metadata']['version']

        self.assertEqual(csv_metadata, d3m_metadata)

    def _test_d3m_saver(self, dataset_path, dataset_name):
        self.maxDiff = None

        try:
            input_dataset_doc_path = dataset_path
            output_dateset_doc_path = os.path.join(self.test_dir, dataset_name, 'datasetDoc.json')

            with open(input_dataset_doc_path, 'r', encoding='utf8') as f:
                input_dataset_description = json.load(f)

            ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=input_dataset_doc_path))
            ds.save('file://{dataset_doc_path}'.format(dataset_doc_path=output_dateset_doc_path))

            with open(output_dateset_doc_path) as f:
                output_dataset_description = json.load(f)

            input_dataset_description = _normalize_dataset_description(input_dataset_description)
            output_dataset_description = _normalize_dataset_description(output_dataset_description, dataset_name)

            self.assertDictEqual(input_dataset_description, output_dataset_description, dataset_name)

            source_files = [x for x in glob.iglob(os.path.join(os.path.dirname(input_dataset_doc_path), '**'), recursive=True)]
            output_files = [x for x in glob.iglob(os.path.join(os.path.dirname(output_dateset_doc_path), '**'), recursive=True)]

            source_relative_filepaths = [os.path.relpath(x, os.path.dirname(input_dataset_doc_path)) for x in source_files]
            output_relative_filepaths = [os.path.relpath(x, os.path.dirname(output_dateset_doc_path)) for x in output_files]

            self.assertEqual(source_relative_filepaths, output_relative_filepaths, dataset_name)

            source_files = [x for x in source_files if (x != input_dataset_doc_path) and os.path.isfile(x)]
            output_files = [x for x in output_files if (x != output_dateset_doc_path) and os.path.isfile(x)]

            for x, y in zip(source_files, output_files):
                if dataset_name == 'dataset_TEST' and os.path.basename(x) == 'learningData.csv':
                    continue

                self.assertTrue(filecmp.cmp(x, y, shallow=False), (dataset_name, x, y))

        finally:
            shutil.rmtree(os.path.join(self.test_dir, dataset_name), ignore_errors=True)

    def test_d3m_preserve_edge_list_resource_type(self):
        source_dataset_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'graph_dataset_2'))
        source_dataset_uri = 'file://{dataset_path}'.format(dataset_path=os.path.join(source_dataset_path, 'datasetDoc.json'))

        output_dataset_path = os.path.join(self.test_dir, 'graph_dataset_2')
        output_dataset_uri = 'file://{dataset_path}'.format(dataset_path=os.path.join(output_dataset_path, 'datasetDoc.json'))

        ds_1 = dataset.Dataset.load(source_dataset_uri)
        ds_1.save(output_dataset_uri)
        ds_2 = dataset.Dataset.load(output_dataset_uri)

        selector = ('edgeList',)

        self.assertEqual(
            convert_metadata(ds_1.metadata.query(selector)),
            convert_metadata(ds_2.metadata.query(selector))
        )

    def test_d3m_saver_synthetic_dataset(self):
        dataset_path = os.path.abspath(os.path.join(self.test_dir, 'synthetic_dataset_1', 'datasetDoc.json'))
        dataset_uri = 'file://{dataset_path}'.format(dataset_path=dataset_path)

        df = pandas.DataFrame([[0]], columns=['col_1'], generate_metadata=False)
        ds = dataset.Dataset(resources={'someData': df}, generate_metadata=True)

        with self.assertRaises(exceptions.InvalidMetadataError):
            ds.save(dataset_uri)

        ds.metadata = ds.metadata.update((), {
            'id': 'synthetic_dataset_1',
            'name': 'Synthetic dataset 1',
        })

        ds.save(dataset_uri)
        ds2 = dataset.Dataset.load(dataset_uri)

        self.assertEqual(ds2.metadata.to_internal_json_structure(), [{
            'selector': [],
            'metadata': {
                'digest': '4757081eb5663c5027a6ff8711b825aa268c5685362d875c2c33e6a9bf1dbeb2',
                'dimension': {
                    'length': 1,
                    'name': 'resources',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
                },
                'id': 'synthetic_dataset_1',
                'location_uris': [dataset_uri],
                'name': 'Synthetic dataset 1',
                'schema': 'https://metadata.datadrivendiscovery.org/schemas/v0/container.json',
                'source': {'redacted': False},
                'structural_type': 'd3m.container.dataset.Dataset',
                'version': '1.0',
            },
        }, {
            'selector': ['someData'],
            'metadata': {
                'dimension': {
                    'length': 1,
                    'name': 'rows',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                },
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/Table'],
                'structural_type': 'd3m.container.pandas.DataFrame',
            },
        }, {
            'selector': ['someData', '__ALL_ELEMENTS__'],
            'metadata': {
                'dimension': {
                    'length': 1,
                    'name': 'columns',
                    'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                },
            },
        }, {
            'selector': ['someData', '__ALL_ELEMENTS__', 0],
            'metadata': {
                'name': 'col_1',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/UnknownType'],
                'structural_type': 'str',
            },
        }])

    def test_d3m_saver_unknown_type(self):
        metadata = metadata_base.DataMetadata()

        metadata = metadata.update((), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': dataset.container_pandas.DataFrame,
            'id': 'multi_source_1',
            'version': '1.0',
            'name': 'A multi source dataset',
            'source': {
                'license': 'CC0',
                'redacted': False,
                'human_subjects_research': False,
            },
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/DatasetResource',
                    'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            }
        })

        metadata = metadata.update(('learningData',), {
            'structural_type': dataset.container_pandas.DataFrame,
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 3,
            }
        })

        metadata = metadata.update(('learningData', metadata_base.ALL_ELEMENTS), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 3,
            }
        })

        metadata = metadata.update(('learningData', metadata_base.ALL_ELEMENTS, 0), {
            'name': 'd3mIndex',
            'structural_type': str,
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        metadata = metadata.update(('learningData', metadata_base.ALL_ELEMENTS, 1), {
            'name': 'sepalLength',
            'structural_type': str,
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/UnknownType',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        })

        metadata = metadata.update(('learningData', metadata_base.ALL_ELEMENTS, 2), {
            'name': 'species',
            'structural_type': str,
            'semantic_types': [
                'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/UnknownType',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
            ],
        })

        dataset_path = os.path.abspath(os.path.join(self.test_dir, 'unknown_columns_1', 'datasetDoc.json'))
        dataset_uri = 'file://{dataset_path}'.format(dataset_path=dataset_path)

        df = dataset.container_pandas.DataFrame([[0, .1, 'Iris-setosa']], columns=['d3mIndex', 'sepalLength', 'species'])
        ds = dataset.Dataset(resources={'learningData': df}, metadata=metadata)

        with self.assertRaises(exceptions.InvalidMetadataError):
            ds.save(dataset_uri)

        metadata = metadata.update(('learningData', metadata_base.ALL_ELEMENTS, 2), {
            'name': 'species',
            'structural_type': str,
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/UnknownType',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
            ],
        })

        ds = dataset.Dataset(resources={'learningData': df}, metadata=metadata)
        ds.save(dataset_uri)

        ds = dataset.Dataset.load(dataset_uri)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 1))), {
            'name': 'sepalLength',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/UnknownType',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 2))), {
            'name': 'species',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/UnknownType',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        })

    def test_d3m_saver_multi_source(self):
        shutil.copytree(os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'raw_dataset_1')), os.path.join(self.test_dir, 'raw_dataset_1'))
        shutil.copytree(os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'image_dataset_1')), os.path.join(self.test_dir, 'image_dataset_1'))
        shutil.copytree(os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'image_dataset_1')), os.path.join(self.test_dir, 'image_dataset_2'))

        metadata = metadata_base.DataMetadata()

        metadata = metadata.update((), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': dataset.container_pandas.DataFrame,
            'id': 'multi_source_1',
            'version': '1.0',
            'name': 'A multi source dataset',
            'source': {
                'license': 'CC0',
                'redacted': False,
                'human_subjects_research': False,
            },
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            }
        })

        metadata = metadata.update(('learningData',), {
            'structural_type': dataset.container_pandas.DataFrame,
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/FilesCollection',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 3,
            }
        })

        metadata = metadata.update(('learningData', metadata_base.ALL_ELEMENTS), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 1,
            }
        })

        metadata = metadata.update(('learningData', metadata_base.ALL_ELEMENTS, 0), {
            'media_types': ['image/jpeg', 'image/png', 'text/csv'],
            'name': 'filename',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
                'https://metadata.datadrivendiscovery.org/types/FileName',
                'https://metadata.datadrivendiscovery.org/types/UnspecifiedStructure',
            ],
            'structural_type': str,
        })

        metadata = metadata.update(('learningData', 0, 0), {
            'location_base_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=os.path.join(self.test_dir, 'raw_dataset_1', 'raw') + '/')
            ],
            'media_types': ['text/csv'],
        })

        metadata = metadata.update(('learningData', 1, 0), {
            'location_base_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=os.path.join(self.test_dir, 'image_dataset_1', 'media') + '/')
            ],
            'media_types': ['image/jpeg'],
        })

        metadata = metadata.update(('learningData', 2, 0), {
            'location_base_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=os.path.join(self.test_dir, 'image_dataset_2', 'media') + '/')
            ],
            'media_types': ['image/jpeg'],
        })

        df = dataset.container_pandas.DataFrame({'filename': ['complementaryData.csv', 'cifar10_bird_1.png', 'cifar10_bird_1.png']})

        ds = dataset.Dataset(resources={'learningData': df}, metadata=metadata)
        data_path = os.path.abspath(os.path.join(self.test_dir, 'multi_source_1', 'datasetDoc.json'))
        ds.save('file://' + data_path)

        self.assertTrue(os.path.exists(data_path))
        with open(data_path, 'r', encoding='utf') as data_file:
            description = json.load(data_file)

        self.assertEqual(description['dataResources'], [{
            'resID': 'learningData',
            'isCollection': True,
            'resFormat': ['image/jpeg', 'image/png', 'text/csv'],
            'resType': 'raw',
            'resPath': 'files/',
        }])

        self.assertTrue(os.path.exists(os.path.join(self.test_dir, 'multi_source_1', 'files', 'complementaryData.csv')))
        self.assertTrue(os.path.exists(os.path.join(self.test_dir, 'multi_source_1', 'files', 'cifar10_bird_1.png')))
        self.assertTrue(os.path.exists(os.path.join(self.test_dir, 'multi_source_1', 'files', 'cifar10_bird_1.png')))

    def test_csv(self):
        dataset_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'tables', 'learningData.csv'))

        dataset_id = '219a5e7b-4499-4160-9b72-9cfa53c4924d'
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load('file://{dataset_path}'.format(dataset_path=dataset_path), dataset_id=dataset_id, dataset_name=dataset_name)

        self._test_csv(ds, dataset_path, dataset_id, dataset_name)

    def _test_csv(self, ds, dataset_path, dataset_id, dataset_name):
        ds.metadata.check(ds)

        for row in ds['learningData']:
            for cell in row:
                # Nothing should be parsed from a string.
                self.assertIsInstance(cell, str)

        self.assertEqual(len(ds['learningData']), 150, dataset_name)
        self.assertEqual(len(ds['learningData'].iloc[0]), 6, dataset_name)

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': dataset_id,
            'name': dataset_name,
            'stored_size': 4961,
            'location_uris': [
                'file://localhost{dataset_path}'.format(dataset_path=dataset_path),
            ],
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': 'a5e827f2fb60639f1eb7b9bd3b849b0db9c308ba74d0479c20aaeaad77ccda48',
        }, dataset_name)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        }, dataset_name)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 6,
            }
        }, dataset_name)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/UnknownType'],
            'structural_type': 'str',
        }, dataset_name)

        for i in range(1, 5):
            self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, i))), {
                'name': ['sepalLength', 'sepalWidth', 'petalLength', 'petalWidth'][i - 1],
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/UnknownType'],
                'structural_type': 'str',
            }, dataset_name)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 5))), {
            'name': 'species',
            'semantic_types': ['https://metadata.datadrivendiscovery.org/types/UnknownType'],
            'structural_type': 'str',
        }, dataset_name)

    def test_csv_lazy(self):
        dataset_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'tables', 'learningData.csv'))

        dataset_id = '219a5e7b-4499-4160-9b72-9cfa53c4924d'
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load('file://{dataset_path}'.format(dataset_path=dataset_path), dataset_id=dataset_id, dataset_name=dataset_name, lazy=True)

        ds.metadata.check(ds)

        self.assertTrue(len(ds) == 0)
        self.assertTrue(ds.is_lazy())

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': dataset_id,
            'name': dataset_name,
            'location_uris': [
                'file://localhost{dataset_path}'.format(dataset_path=dataset_path),
            ],
            'dimension': {
                'length': 0,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {})

        ds.load_lazy()

        self.assertFalse(ds.is_lazy())

        self._test_csv(ds, dataset_path, dataset_id, dataset_name)

    def test_sklearn(self):
        for dataset_path in ['boston', 'breast_cancer', 'diabetes', 'digits', 'iris', 'linnerud']:
            dataset.Dataset.load('sklearn://{dataset_path}'.format(dataset_path=dataset_path))

        dataset_uri = 'sklearn://iris'
        dataset_id = str(uuid.uuid3(uuid.NAMESPACE_URL, dataset_uri))
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load(dataset_uri, dataset_id=dataset_id, dataset_name=dataset_name)

        self._test_sklearn(ds, dataset_uri)

    def _test_sklearn(self, ds, dataset_uri):
        ds.metadata.check(ds)

        self.assertEqual(len(ds['learningData']), 150, dataset_uri)
        self.assertEqual(len(ds['learningData'].iloc[0]), 5, dataset_uri)

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': '44f6efaa-72e7-383e-9369-64bd7168fb26',
            'name': 'Iris Dataset',
            'location_uris': [
                dataset_uri,
            ],
            'description': datasets.load_iris()['DESCR'],
            'dimension': {
                'length': 1,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': '2cd0dd490ba383fe08a9f89514f6688bb5cb77d4a7da140e9458e7c534eb82f4',
        }, dataset_uri)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 150,
            }
        }, dataset_uri)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 5,
            }
        }, dataset_uri)

        for i in range(0, 4):
            self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, i))), {
                'name': ['sepal length (cm)', 'sepal width (cm)', 'petal length (cm)', 'petal width (cm)'][i],
                'structural_type': 'numpy.float64',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/UnknownType',
                    'https://metadata.datadrivendiscovery.org/types/Attribute',
                ],
            }, dataset_uri)

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 4))), {
            'name': 'column 4',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        }, dataset_uri)

    @unittest.skip("requires rewrite")
    # TODO: Fix. Currently "generate_metadata" is not called when not loading lazily.
    #       We should just always use auto generation for as much as possible.
    #       Or not, to make sure things are speedy?
    def test_sklearn_lazy(self):
        for dataset_path in ['boston', 'breast_cancer', 'diabetes', 'digits', 'iris', 'linnerud']:
            dataset.Dataset.load('sklearn://{dataset_path}'.format(dataset_path=dataset_path))

        dataset_uri = 'sklearn://iris'
        dataset_id = str(uuid.uuid3(uuid.NAMESPACE_URL, dataset_uri))
        dataset_name = 'Iris Dataset'

        ds = dataset.Dataset.load(dataset_uri, dataset_id=dataset_id, dataset_name=dataset_name, lazy=True)

        ds.metadata.check(ds)

        self.assertTrue(len(ds) == 0)
        self.assertTrue(ds.is_lazy())

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': '44f6efaa-72e7-383e-9369-64bd7168fb26',
            'name': 'Iris Dataset',
            'location_uris': [
                dataset_uri,
            ],
            'dimension': {
                'length': 0,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {})
        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {})

        ds.load_lazy()

        self.assertFalse(ds.is_lazy())

        self._test_sklearn(ds, dataset_uri)

    def test_multi_table(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'database_dataset_1', 'datasetDoc.json'))

        dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

    def test_timeseries(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'timeseries_dataset_1', 'datasetDoc.json'))

        dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

    def test_audio(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'audio_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self.maxDiff = None

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': metadata_base.CONTAINER_SCHEMA_VERSION,
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'audio_dataset_1',
            'version': '1.0',
            'name': 'Audio dataset to be used for tests',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'source': {
                'license': 'CC0',
                'redacted': False,
            },
            'dimension': {
                'length': 2,
                'name': 'resources',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/DatasetResource'],
            },
            'digest': '2e1ca903305f0e71f8b6ab5f81d942e0335e6fe5ae901b311b1bcd1aed9a5fe1',
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/FilesCollection',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 1,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 1,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData',))), {
            'structural_type': 'd3m.container.pandas.DataFrame',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/Table',
                'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint',
            ],
            'dimension': {
                'name': 'rows',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularRow'],
                'length': 1,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS))), {
            'dimension': {
                'name': 'columns',
                'semantic_types': ['https://metadata.datadrivendiscovery.org/types/TabularColumn'],
                'length': 5,
            }
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 0))), {
            'name': 'd3mIndex',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Integer',
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
            ],
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 1))), {
            'name': 'audio_file',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Text',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
            'foreign_key': {
                'type': 'COLUMN',
                'resource_id': '0',
                'column_index': 0,
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 2))), {
            'name': 'start',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/Boundary',
                'https://metadata.datadrivendiscovery.org/types/IntervalStart',
            ],
            'boundary_for': {
                'resource_id': 'learningData',
                'column_index': 1,
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 3))), {
            'name': 'end',
            'structural_type': 'str',
            'semantic_types': [
                'http://schema.org/Float',
                'https://metadata.datadrivendiscovery.org/types/Boundary',
                'https://metadata.datadrivendiscovery.org/types/IntervalEnd',
            ],
            'boundary_for': {
                'resource_id': 'learningData',
                'column_index': 1,
            },
        })

        self.assertEqual(convert_metadata(ds.metadata.query(('learningData', metadata_base.ALL_ELEMENTS, 4))), {
            'name': 'class',
            'structural_type': 'str',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/CategoricalData',
                'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                'https://metadata.datadrivendiscovery.org/types/Attribute',
            ],
        })

    def test_raw(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'raw_dataset_1', 'datasetDoc.json'))

        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self.assertEqual(convert_metadata(ds.metadata.query(())), {
            'schema': 'https://metadata.datadrivendiscovery.org/schemas/v0/container.json',
            'structural_type': 'd3m.container.dataset.Dataset',
            'id': 'raw_dataset_1',
            'version': '1.0',
            'name': 'Raw dataset to be used for tests',
            'location_uris': [
                'file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path),
            ],
            'dimension': {
                'name': 'resources',
                'semantic_types': [
                    'https://metadata.datadrivendiscovery.org/types/DatasetResource'
                ],
                'length': 1
            },
            'digest': '125ce6397279d48df31e36a1ce0fad80f7cea9ed689aad7c87e860b8225b8e95',
            'source': {
                'redacted': False,
            }
        })
        self.assertEqual(convert_metadata(ds.metadata.query(('0', metadata_base.ALL_ELEMENTS, 0))), {
            'location_base_uris': [
                'file://{dataset_path}/raw/'.format(dataset_path=os.path.dirname(dataset_doc_path)),
            ],
            'media_types': ['text/csv'],
            'name': 'filename',
            'semantic_types': [
                'https://metadata.datadrivendiscovery.org/types/PrimaryKey',
                'https://metadata.datadrivendiscovery.org/types/FileName',
                'https://metadata.datadrivendiscovery.org/types/UnspecifiedStructure',
            ],
            'structural_type': 'str',
        })

    def test_select_rows(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'iris_dataset_1', 'datasetDoc.json'))
        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        # add metadata for rows 0, 1, 2
        ds.metadata = ds.metadata.update(('learningData', 0), {'a': 0})
        ds.metadata = ds.metadata.update(('learningData', 1), {'b': 1})
        ds.metadata = ds.metadata.update(('learningData', 2), {'c': 2})

        cut_dataset = ds.select_rows({'learningData': [0, 2]})

        # verify that rows are removed from dataframe and re-indexed
        self.assertListEqual([0, 1], list(cut_dataset['learningData'].index))
        self.assertListEqual(['0', '2'], list(cut_dataset['learningData'].d3mIndex))

        # verify that metadata is removed and re-indexed
        self.assertEqual(cut_dataset.metadata.query(('learningData', 0))['a'], 0)
        self.assertEqual(cut_dataset.metadata.query(('learningData', 1))['c'], 2)

    def test_score_workaround(self):
        dataset_doc_path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'data', 'datasets', 'score_dataset_1', 'dataset_TEST', 'datasetDoc.json'))
        ds = dataset.Dataset.load('file://{dataset_doc_path}'.format(dataset_doc_path=dataset_doc_path))

        self.assertEqual(ds.metadata.query_field((), 'id'), 'object_dataset_1_SCORE')

        self.assertEqual(ds['learningData'].values.tolist(), [
            ['0', 'img_00285.png', 'red', '480,457,480,529,515,529,515,457'],
            ['0', 'img_00285.png', 'black', '10,117,10,329,105,329,105,117'],
            ['1', 'img_00225.png', 'blue', '422,540,422,660,576,660,576,540'],
            ['1', 'img_00225.png', 'red', '739,460,739,545,768,545,768,460'],
        ])


if __name__ == '__main__':
    unittest.main()
